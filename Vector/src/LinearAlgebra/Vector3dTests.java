//Thomas Comeau 1934037
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testCons() {
		Vector3d vec = new Vector3d(1,2,3);
		assertEquals(1,vec.getX());
		assertEquals(2,vec.getY());
		assertEquals(3,vec.getZ());
		Vector3d vec2 = new Vector3d(56,33,300);
		assertEquals(56,vec2.getX());
		assertEquals(33,vec2.getY());
		assertEquals(300,vec2.getZ());
	}
	
	@Test
	void testMang() {
		Vector3d vec = new Vector3d(1,2,3);
		assertEquals(3.7416573867739413,vec.magnitude());
		Vector3d vec2 = new Vector3d(100,22,76);
		assertEquals(127.51470503436065,vec2.magnitude());
	}
	
	@Test
	void testDotproduct() {
		Vector3d vec = new Vector3d(1,2,3);
		Vector3d vec2 = new Vector3d(4,5,6);
		assertEquals(32,vec.dotProduct(vec2));
		Vector3d vec3 = new Vector3d(10,52,93);
		Vector3d vec4 = new Vector3d(42,15,86);
		assertEquals(9198,vec3.dotProduct(vec4));
	}
	
	@Test
	void testAdd() {
		Vector3d vec = new Vector3d(1,2,3);
		Vector3d vec2 = new Vector3d(4,5,6);
		Vector3d vec3 = vec.add(vec2);
		assertEquals(5,vec3.getX());
		assertEquals(7,vec3.getY());
		assertEquals(9,vec3.getZ());
		Vector3d vec4 = new Vector3d(15,82,43);
		Vector3d vec5 = new Vector3d(43,51,26);
		Vector3d vec6 = vec4.add(vec5);
		assertEquals(58,vec6.getX());
		assertEquals(133,vec6.getY());
		assertEquals(69,vec6.getZ());
	}
}
