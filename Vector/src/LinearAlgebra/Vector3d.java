//Thomas Comeau 1934037
package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x1, double y1, double z1) {
		x = x1;
		y = y1;
		z = z1;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public double magnitude() {
		double m;
		m = Math.sqrt(Math.pow(x,2)+Math.pow(y,2)+Math.pow(z,2));
		return m;
	}
	
	public double dotProduct(Vector3d v) {
		double dotp;
		dotp = (x * v.getX()) + (y * v.getY()) + (z * v.getZ());
		return dotp;
	}
	
	public Vector3d add(Vector3d v) {
		double x1;
		double y1;
		double z1;
		x1 = x + v.getX();
		y1 = y + v.getY();
		z1 = z + v.getZ();
		Vector3d vec = new Vector3d(x1,y1,z1);
		return vec;
	}
}
