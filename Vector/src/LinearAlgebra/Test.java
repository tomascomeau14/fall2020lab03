//Thomas Comeau 1934037
package LinearAlgebra;

public class Test {

	public static void main(String[] args) {
		Vector3d vec = new Vector3d(1,2,3);
		System.out.println("Mag "+vec.magnitude());
		Vector3d vec2 = new Vector3d(4,5,6);
		System.out.println("Dotproduct "+vec.dotProduct(vec2));
		Vector3d vec3 = vec.add(vec2);
		System.out.println(vec3.getX());
		System.out.println(vec3.getY());
		System.out.println(vec3.getZ());
	}

}
